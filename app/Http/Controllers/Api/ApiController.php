<?php

namespace App\Http\Controllers\Api;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class ApiController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function methods()
    {
    
        $routeCollections = \Route::getRoutes();
        $uris = [];
        foreach ($routeCollections as $route) {
            if (preg_match('/^api*/', $route->uri())) {
                $uris[$route->uri()] = [
                  'name' => $route->uri(),
                  'methods' => $route->methods(),
                  'action'  => $route->getAction()
                ];
            }
        }
        $methods =[
          'local' => $uris,
        ];
        return response()->json($methods);
    }
}
