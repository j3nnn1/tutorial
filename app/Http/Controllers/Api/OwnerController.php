<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\ApiController;
use App\Owner;
use Exception;

class OwnerController extends ApiController
{
    public function __construct(Owner $owner) {
        $this->owner = $owner;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Owner $owner)
    {
        return response()->json(['owners' => $this->owner->listAll()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $owners = $request->all();
            foreach ($owner as $ow) {
                $owner[] = $this->owner->firstOrCreate($ow);
            }
            return response()->json(['owner'=>$owner]);
        } catch (\Exception $e) {
            return response()->json(['owner'=>[], 'error'=>$e->getMessage()], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       try {
            $owner = $this->owner->where(['id'=>$id])->with('pets')->first();
            return response()->json(['owner'=>$owner]);
       } catch (\Exception $e) {
            return response()->json(['owner'=>[], 'error'=>$e->getMessage()], 400);
       } 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $owner = $this->owner->find($id);
            $attributes = $request->all();
            foreach ($attributes as $attr=>$val) {
                $owner->$attr = $val;
            }
            $owner->save();
            return response()->json(['owner'=>$owner]);
        } catch (\Exception $e) {
            return response()->json(['owner'=>[], 'error'=>$e->getMessage()], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
