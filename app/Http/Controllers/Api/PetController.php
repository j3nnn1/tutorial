<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller\Api\ApiController;
use App\Pet;
use Exception;

class PetController extends ApiController
{
    public $pet = null;

    public function __construct(Pet $pet) {
        $this->pet = $pet;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(['pets' => $this->pet->all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $pets = $request->all();
            foreach ($pets as $idx =>$pt) {
                if (empty($pt['owner_id'])) {
                    Throw new Exception('Owner Id debe ser definido en '.$pt['name']. ' registro '.$idx, 1);
                }
                if (empty($pt['pet_type_id'])) {
                    Throw new Exception('Pet Type Id debe ser definido en '.$pt['name']. ' registro '.$idx, 1);
                }
                $pet[] = $this->pet->firstOrCreate($pt);
            }
            return response()->json(['pet'=>$pet]);
        } catch (\Exception $e) {
            return response()->json(['pet'=>[], 'error'=>$e->getMessage()], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            return response()->json(['pet'=> (boolean) $this->pet->destroy($id)]);
        } catch (\Exception $e) {
            return response()->json(['pet'=>[], 'error'=>$e->getMessage()], 400);
        }
    }
}
