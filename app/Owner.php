<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Owner extends Model
{
    public $fillable = [
        'name',
        'lastname',
        'birthday'
    ];
    public function pets()
    {
        return $this->hasMany('App\Pet', 'owner_id');  
    }
    public function listAll()
    {
        return $this->get([
             \DB::raw('TIMESTAMPDIFF(YEAR, owners.birthday, CURDATE()) as age'),
             'owners.name',
             'owners.lastname',
             'owners.birthday',
             'owners.created_at',
             'owners.updated_at'             
        ]);
    }
}
