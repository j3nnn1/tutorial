<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pet extends Model
{
     public $fillable = [
        'name',
        'gender',
        'birthday',
        'owner_id',
        'pet_type_id'
    ];
    public function owner()
    {
        return $this->belongsTo('App\Owner');
    }

}
