<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePet extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pets', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable(false);
            $table->enum('gender', ['female', 'male']);
            $table->dateTime('birthday');
            $table->integer('owner_id')->unsigned();
            $table->integer('pet_type_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pets');
    }
}
