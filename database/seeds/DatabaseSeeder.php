<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PetTypesSeeder::class);
        $this->call(OwnersSeeder::class);
        $this->call(PetsSeeder::class);
    }
}
