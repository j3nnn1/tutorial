<?php
use Illuminate\Database\Seeder;
use App\Owner;

class OwnersSeeder extends Seeder
{

    public function run()
    {
        $this->loadOwnersRows();
    }
    public function loadOwnersRows()
    {
       DB::table('owners')->delete();  
       Owner::create(['id'=>1, 'name'=>'bob', 'lastname'=>'cruz', 'birthday'=> \Carbon\Carbon::now()->toDateTimeString()]);
       Owner::create(['id'=>2, 'name'=>'alice', 'lastname'=>'domokun', 'birthday'=> \Carbon\Carbon::now()->toDateTimeString()]);
       Owner::create(['id'=>3, 'name'=>'eulalia', 'lastname'=>'panvini', 'birthday'=> \Carbon\Carbon::now()->toDateTimeString()]);
    }
}
