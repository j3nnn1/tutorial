<?php

use Illuminate\Database\Seeder;
use App\PetType;

class PetTypesSeeder extends Seeder
{

    public function run()
    {
        $this->loadPetTypesRows();
    }
    public function loadPetTypesRows()
    {
       DB::table('pet_types')->delete();  
       PetType::create(['id'=>1, 'name'=>'perro']);
       PetType::create(['id'=>2, 'name'=>'gato']);
       PetType::create(['id'=>3, 'name'=>'pez']);
    }
}
