<?php
use Illuminate\Database\Seeder;
use App\Pet;

class PetsSeeder extends Seeder
{

    public function run()
    {
        $this->loadPetRows();
    }
    public function loadPetRows()
    {
       DB::table('pets')->delete();  
       Pet::create(['id'=>1, 'name'=>'cotufa', 'gender'=>'female', 'birthday'=>\Carbon\Carbon::now()->toDateTimeString(), 'owner_id'=>1, 'pet_type_id'=>1]);
    }
}
