<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('pet', 'Api\PetController');
Route::resource('owner', 'Api\OwnerController');
Route::patch('owner/{id}', 'Api\OwnerController@update');
Route::put('owner/{id}', 'Api\OwnerController@update');
#Route::g('owner/{ownerId}/pet', 'Api\OwnerPetController');

Route::get('/methods', 'Api\ApiController@methods');

